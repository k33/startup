'use strict';

const gulp = require('gulp');
const sass = require('gulp-sass');
const sourcemaps = require('gulp-sourcemaps');
const autoprefixer = require('autoprefixer');
const postcss = require('gulp-postcss');
const lost = require('lost');

gulp.task('sass', function() {
  return gulp.src([
    'scss/**/*.scss',
  ])
    .pipe(sourcemaps.init())
    .pipe(sass().on('error', sass.logError))
    .pipe(postcss([
      lost(),
      autoprefixer()
    ]))
    .pipe(sourcemaps.write('./maps'))
    .pipe(gulp.dest('css/'));
});

gulp.task('watch', function() {
  gulp.watch('scss/**', ['sass']);
});

gulp.task('default', ['watch', 'sass']);
