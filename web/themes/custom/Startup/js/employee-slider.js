(function ($, Drupal) {
  'use strict';

  Drupal.behaviors.HeaderMenu = {
    attach: function (context, settings) {

      $('.navbar-toggle').on('click', function () {
        var $attrValue = $(this).hasClass('collapsed');
        $('.navbar').toggleClass('set-bg');
        var $heroBlock = $('.hero-block ');
        $heroBlock.find('.field--type-text').toggleClass('hide-content');
        $heroBlock.find('.field--type-link').toggleClass('hide-content');
        $heroBlock.find('.field--name-field-subtitle').toggleClass('hide-content');
      })
    }
  };

}(jQuery, Drupal));
