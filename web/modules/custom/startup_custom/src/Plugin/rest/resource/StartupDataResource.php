<?php

namespace Drupal\startup_custom\Plugin\rest\resource;

use Drupal\rest\Plugin\ResourceBase;
use Drupal\rest\ResourceResponse;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 *
 * @class StartupDataResource
 *
 * @RestResource(
 *   id = "startup_data_reosuce",
 *   label = @Translation("Startup data reosuce"),
 *   uri_paths = {
 *     "create" = "/startup/data/reosuce",
 *   }
 * )
 */
class StartupDataResource extends ResourceBase {

  /**
   * The logger.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $logger;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, array $serializer_formats, LoggerInterface $logger) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $serializer_formats, $logger);
    $this->logger = $logger;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->getParameter('serializer.formats'),
      $container->get('logger.factory')->get('startup_custom')
    );
  }

  /**
   * Responds to POST requests.
   */
  public function post() {
    $request = \Drupal::request()->request;
    $response = [];

    if ($request->get('body')) {
      $this->logger->notice($request->get('boyd'));
      $response = $request->get('boyd');

      return new ResourceResponse($response);
    }

    return new ResourceResponse($response);
  }

}
